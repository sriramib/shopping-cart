//
//  ProductsViewController.m
//  MyNewSampleApp
//
//  Created by Sreenu Bolledla on 10/01/15.
//  Copyright (c) 2015 Sreenu Bolledla. All rights reserved.
//

#import "ProductsViewController.h"

@interface ProductsViewController ()

@end

@implementation ProductsViewController

@synthesize myCollectionView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    productsImages = [[NSMutableArray alloc]initWithObjects:@"elmore1.png",@"elmore2.png",@"elmore3.png",@"elmore4.png",@"elmore5.png",@"elmore6.png",@"elmore7.png",@"elmore8.png",@"elmore9.png",@"elmore10.png", nil];
    
    productDescription = [[NSMutableArray alloc]initWithObjects:@"Elmore Oil is a proven pain reliever and anti inflammatory, which makes it one of the very best options to treat recurring back pain, no matter what the cause. Whether your back pain is caused from muscle strain or from inflammation, Elmore Oil will provide you with fast relief and a good nights sleep. It doesnt matter if its caused by pain radiating from your neck and upper back or a direct problem in the lumbar area, Elmore Oil can provide improved quality of life by relieving the pain and providing you the opportunity to go about your daily activities uninhibited by pain or lack of movement.",
                          @"The Elmore Oil CREAM is a new convenient way to apply your favourite pain reliever. With the same active ingredients of Tea Tree Oil and Eucalyptus (just like the oil formula) but in a more convenient, no mess cream base, that rubs right in with no residue. Just like the oil, the CREAM relieves muscular aches and pains and is perfect for joint pain, including all forms of arthritis. Regular retail price is $ 14.95 per tube, but if you buy the twin pack today you pay only $ 26.90 – SAVE $ 3.00",
                          @"Elmore Oil is a proven pain reliever and anti inflammatory, which makes it one of the very best options to treat recurring back pain, no matter what the cause. Whether your back pain is caused from muscle strain or from inflammation, Elmore Oil will provide you with fast relief and a good nights sleep. It doesnt matter if its caused by pain radiating from your neck and upper back or a direct problem in the lumbar area, Elmore Oil can provide improved quality of life by relieving the pain and providing you the opportunity to go about your daily activities uninhibited by pain or lack of movement.",
                          
                          @"Australia's favorite all natural pain reliver, now available in convenient 3mL sachets. Perfect for travel, handbags, sports clubs or even your pocket!",
                          @"A collection of your favourite ARTHRITIS label, giving you one of every pack size. You receive a 250ml bottle, a 125ml bottle and the handy 50 ml roll-on pack of Elmore Oil for Arthritis. Of course it doesn’t matter if it has the arthritis label or the sports label, it’s the same great product inside, but the directions on the label are just more specific for the condition you are treating.",
                          @"Try the amazing Essentials Body Wash in your choice of essence. You can choose from Olive, Lemon Tea Tree, Eucalyptus or Vanilla. At only $9.95 for each 200ml bottle you will find great value in this high quality product range.",
                          
                          @"The Essentials Body Wash products are high foaming and are designed as a shower soap or hand soap for everyday use. Just a small shot from the easy to use pump will give you a rich foaming lather which is mild to the skin.",
                          @"Try one of our beautiful Moisturising Lotions in your choice of essence. You can choose from Olive, Lemon Tea Tree, Eucalyptus or Vanilla. At just $9.95 for the 200ml lotion you will be amazed at the quality and feel of this exquisite product on your skin.",
                          
                          @"Indulge yourself with these exquisite non-irritant soaps available in your choice of  essence. You can choose from Olive, Lemon Tea Tree, Eucalyptus or Vanilla.",
                          @"Indulge yourself with these exquisite non-irritant soaps available in your choice of  essence. You can choose from Olive, Lemon Tea Tree, Eucalyptus or Vanilla.",
                          @"Indulge yourself with these exquisite non-irritant soaps available in your choice of  essence. You can choose from Olive, Lemon Tea Tree, Eucalyptus or Vanilla.",
                          
                          
                          nil];
    
    
    myCollectionView.dataSource = self;
    myCollectionView.delegate = self;
    
    myCollectionView.backgroundColor = [UIColor whiteColor];
    
   // [myCollectionView registerNib:[UINib nibWithNibName:@"MyCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


- (IBAction)bckBtn1:(id)sender
{
//    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [productsImages count];
    
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MyCollectionViewCell *cell;// = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    
    static NSString *cellIdentifier = @"MyCollectionViewCell";
    
    //  MyCollectionViewCell *cell;
    
    if (cell == nil)
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.imgProduct.image = [UIImage imageNamed:[productsImages objectAtIndex:indexPath.row]];
    
    cell.layer.borderColor=[UIColor grayColor].CGColor;
    cell.layer.borderWidth=1;
    
    cell.newlbl.text = [NSString stringWithFormat:@"%d",indexPath.row];
    NSLog(@"%d indexpath",indexPath.row);
    
    [cell.imgProduct.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [cell.imgProduct.layer setBorderWidth: 0.5];
    //  Above Code is for imageview border in products list
    
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{ UIStoryboard *storyboard = self.storyboard;
    ProductsDetailViewController *svc = [storyboard instantiateViewControllerWithIdentifier:@"ProductsDetailViewController"];
    //[[UIStoryboard storyboardWithName:@"StoryboardNameOfnewSurveyView" bundle:nil] instantiateViewControllerWithIdentifier:@"newSurveyView"];
    
    // Configure the new view controller here.
    [self.navigationController pushViewController:svc animated:YES];
    
    svc.imgData = [productsImages objectAtIndex:indexPath.item];
    svc.productDescp = [productDescription objectAtIndex:indexPath.item];
    
    NSLog(@"cell no is %d ",indexPath.row);
  /*  ProductsDetailViewController *productsDetailViewController= [[ProductsDetailViewController alloc]initWithNibName:@"ProductsDetailViewController" bundle:nil];
    
    
   
    
    switch (indexPath.item) {
        case 0:
        {
            productsDetailViewController.imgData = [productsImages objectAtIndex:indexPath.item];
            productsDetailViewController.productDescp = [productDescription objectAtIndex:indexPath.item];
            
            
            [self presentViewController:productsDetailViewController animated:YES completion:nil];
        }
            
            break;
            
        case 1:
        {
            productsDetailViewController.imgData = [productsImages objectAtIndex:indexPath.item];
            productsDetailViewController.productDescp = [productDescription objectAtIndex:indexPath.item];
            [self presentViewController:productsDetailViewController animated:YES completion:nil];
        }
            
            break;
            
        case 2:
        {
            productsDetailViewController.imgData = [productsImages objectAtIndex:indexPath.item];
            productsDetailViewController.productDescp = [productDescription objectAtIndex:indexPath.item];
            [self presentViewController:productsDetailViewController animated:YES completion:nil];
        }
            
            break;
            
        case 3:
        {
            productsDetailViewController.imgData = [productsImages objectAtIndex:indexPath.item];
            productsDetailViewController.productDescp = [productDescription objectAtIndex:indexPath.item];
            [self presentViewController:productsDetailViewController animated:YES completion:nil];
        }
            
            break;
            
        case 4:
        {
            productsDetailViewController.imgData = [productsImages objectAtIndex:indexPath.item];
            productsDetailViewController.productDescp = [productDescription objectAtIndex:indexPath.item];
            [self presentViewController:productsDetailViewController animated:YES completion:nil];
        }
            
            break;
            
        case 5:
        {
            productsDetailViewController.imgData = [productsImages objectAtIndex:indexPath.item];
            productsDetailViewController.productDescp = [productDescription objectAtIndex:indexPath.item];
            [self presentViewController:productsDetailViewController animated:YES completion:nil];
        }
            
            break;
            
        case 6:
        {
            productsDetailViewController.imgData = [productsImages objectAtIndex:indexPath.item];
            productsDetailViewController.productDescp = [productDescription objectAtIndex:indexPath.item];
            [self presentViewController:productsDetailViewController animated:YES completion:nil];
        }
            
            break;
            
        case 7:
        {
            productsDetailViewController.imgData = [productsImages objectAtIndex:indexPath.item];
            productsDetailViewController.productDescp = [productDescription objectAtIndex:indexPath.item];
            [self presentViewController:productsDetailViewController animated:YES completion:nil];
        }
            
            break;
            
        case 8:
        {
            productsDetailViewController.imgData = [productsImages objectAtIndex:indexPath.item];
            productsDetailViewController.productDescp = [productDescription objectAtIndex:indexPath.item];
            [self presentViewController:productsDetailViewController animated:YES completion:nil];
        }
            
            break;
            
        case 9:
        {
            productsDetailViewController.imgData = [productsImages objectAtIndex:indexPath.item];
            productsDetailViewController.productDescp = [productDescription objectAtIndex:indexPath.item];
            [self presentViewController:productsDetailViewController animated:YES completion:nil];
        }
            
            break;
            
    
        default:
            break;
    }*/
}

@end
