//
//  ProductCartCell.h
//  DynamicsDemo
//
//  Created by Sreenu Bolledla on 22/04/15.
//  Copyright (c) 2015 Gabriel Theodoropoulos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCartCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *productImage;
@property (strong, nonatomic) IBOutlet UILabel *productTitle;
@property (strong, nonatomic) IBOutlet UILabel *productPrice;
@property (strong, nonatomic) IBOutlet UILabel *quantity;

@property (strong, nonatomic) IBOutlet UIButton *deleteButton;




@end
