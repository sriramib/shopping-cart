//
//  ProductsViewController.h
//  MyNewSampleApp
//
//  Created by Sreenu Bolledla on 10/01/15.
//  Copyright (c) 2015 Sreenu Bolledla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyCollectionViewCell.h"
#import "ProductsDetailViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ProductsViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSMutableArray *productsImages;
    NSMutableArray *productDescription;
    
}

- (IBAction)bckBtn1:(id)sender;

@property (strong, nonatomic) IBOutlet UICollectionView *myCollectionView;



@end
