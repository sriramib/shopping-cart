//
//  ProductsDetailViewController.m
//  MyNewSampleApp
//
//  Created by Sreenu Bolledla on 10/01/15.
//  Copyright (c) 2015 Sreenu Bolledla. All rights reserved.
//

#import "ProductsDetailViewController.h"

@interface ProductsDetailViewController ()

@end

@implementation ProductsDetailViewController
@synthesize imgDetail,imgData,productDescp,txtViewDescp;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    imgDetail.image = [UIImage imageNamed:imgData];
    txtViewDescp.text = productDescp;
    
    UIBarButtonItem *button = [[UIBarButtonItem alloc]
                               initWithImage:[UIImage imageNamed:@"shopping_cart_empty"] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.rightBarButtonItem = button;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBack:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)segmentChange:(id)sender
{
    
    UISegmentedControl *seg = sender;
    if(seg.selectedSegmentIndex==0)
    {
        NSLog(@"Wishlist");
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Product added to your wishlist.."
                                                       message: nil
                                                      delegate: self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        
        [alert setTag:1];
        [alert show];
    }
    if(seg.selectedSegmentIndex==1)
    {
        
        NSLog(@"Buy Now");
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Product added to your cart.."
                                                       message: nil
                                                      delegate: self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        
        [alert setTag:2];
        [alert show];
       
    }

}

- (IBAction)wishlistbtn:(id)sender
{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Product added to your wishlist.."
                                                   message: nil
                                                  delegate: self
                                         cancelButtonTitle:nil
                                         otherButtonTitles:@"OK",nil];
    [alert show];

}

- (IBAction)buyNowbtn:(id)sender
{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Product added to your cart.."
                                                   message: nil
                                                  delegate: self
                                         cancelButtonTitle:nil
                                         otherButtonTitles:@"OK",nil];
    
    
    [alert show];

}
@end







