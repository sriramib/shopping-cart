//
//  MyCollectionViewCell.h
//  MyNewSampleApp
//
//  Created by Sreenu Bolledla on 10/01/15.
//  Copyright (c) 2015 Sreenu Bolledla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgProduct;

@property (strong, nonatomic) IBOutlet UILabel *newlbl;

@end
