//
//  ProductCartCell.m
//  DynamicsDemo
//
//  Created by Sreenu Bolledla on 22/04/15.
//  Copyright (c) 2015 Gabriel Theodoropoulos. All rights reserved.
//

#import "ProductCartCell.h"

@implementation ProductCartCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    
    
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
