//
//  ProductsDetailViewController.h
//  MyNewSampleApp
//
//  Created by Sreenu Bolledla on 10/01/15.
//  Copyright (c) 2015 Sreenu Bolledla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductsDetailViewController : UIViewController


@property (strong, nonatomic) NSString *imgData;
@property(strong, nonatomic) NSString *productDescp;

@property (strong, nonatomic) IBOutlet UIImageView *imgDetail;
@property (strong, nonatomic) IBOutlet UITextView *txtViewDescp;
- (IBAction)btnBack:(id)sender;

- (IBAction)segmentChange:(id)sender;
- (IBAction)wishlistbtn:(id)sender;
- (IBAction)buyNowbtn:(id)sender;


@end
