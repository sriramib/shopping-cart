//
//  CartViewController.h
//  DynamicsDemo
//
//  Created by Sreenu Bolledla on 22/04/15.
//  Copyright (c) 2015 Gabriel Theodoropoulos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (void)loadItems;

@end
