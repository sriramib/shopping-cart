//
//  ViewController.m
//  DynamicsDemo
//
//  Created by Gabriel Theodoropoulos on 31/3/14.
//  Copyright (c) 2014 Gabriel Theodoropoulos. All rights reserved.
//

#import "ViewController.h"
#import "MenuComponent.h"
#import "AlertComponent.h"
#import "ProductsDetailViewController.h"
#import "ProductsViewController.h"
#import "MyCollectionViewCell.h"



@interface ViewController ()
{
    BOOL menuButton;
    UISwipeGestureRecognizer *showMenuGesture ;
}

@property (nonatomic, strong) MenuComponent *menuComponent;

-(void)showMenu:(UIGestureRecognizer *)gestureRecognizer;

@property (nonatomic, strong) AlertComponent *alertComponent;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    menuButton = YES;
    
    // Setup a swipe gesture.
    showMenuGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(showMenu:)];
    
    showMenuGesture.direction = UISwipeGestureRecognizerDirectionRight;
    
   // [self.view addGestureRecognizer:showMenuGesture];
    
    
    //UITapGestureRecognizer *singleFingerTap =
    //[[UITapGestureRecognizer alloc] initWithTarget:self
     //                                       action:@selector(handleSingleTap:)];
    //[self.view addGestureRecognizer:singleFingerTap];
    //[singleFingerTap release];
    
    
    
    // Initialize the menu component.
    CGRect desiredMenuFrame = CGRectMake(0.0, 20.0, 150.0, self.view.frame.size.height);
    self.menuComponent = [[MenuComponent alloc] initMenuWithFrame:desiredMenuFrame
                                                       targetView:self.view
                                                        direction:menuDirectionLeftToRight
                                                          options:@[@"Home", @"My Cart", @"E-mail", @"Settings", @"About",@"Crash"]
                                                     optionImages:@[@"homeIcon3.png", @"upload", @"email", @"settings", @"info",@"info"]];
    
    
    // Initialize the alert view component.
    self.alertComponent = [[AlertComponent alloc] initAlertWithTitle:@"Custom Alert"
                                                          andMessage:@"You have a new e-mail message, but I don't know from whom."
                                                     andButtonTitles:@[@"Show me", @"I don't care", @"For me, really?"]
                                                       andTargetView:self.view];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    NSLog(@"tap gesture");
    
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Private method implementation.

-(void)showMenu:(UIGestureRecognizer *)gestureRecognizer{
    
     menuButton = NO;
    [self.menuComponent showMenuWithSelectionHandler:^(NSInteger selectedOptionIndex) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"UIKit Dynamics Menu"
//                                                        message:[NSString stringWithFormat:@"You selected option #%d", selectedOptionIndex + 1]
//                                                       delegate:nil
//                                              cancelButtonTitle:nil
//                                              otherButtonTitles:@"Okay", nil];
//        [alert show];
        
        menuButton = YES;
        
        [self.menuComponent toggleMenu];
        self.menuComponent.isMenuShown = NO;
        
        switch (selectedOptionIndex) {
            case 0:
                [self callmyview:selectedOptionIndex];
                break;
                
            case 1:
                [self mycartview:selectedOptionIndex];
                break;
                
            default:
                break;
        }
        
        
        
    }];
}
-(void)callmyview:(NSInteger *)index
{
   
    NSLog(@"index is %ld",index);
    UIStoryboard *storyboard = self.storyboard;
    ProductsViewController *svc = [storyboard instantiateViewControllerWithIdentifier:@"ProductsViewController"];
    //[[UIStoryboard storyboardWithName:@"StoryboardNameOfnewSurveyView" bundle:nil] instantiateViewControllerWithIdentifier:@"newSurveyView"];

    // Configure the new view controller here.
    [self.navigationController pushViewController:svc animated:YES];
  //  [self presentViewController:svc animated:YES completion:nil];
}

-(void)mycartview:(NSInteger *)index
{
    NSLog(@"mycartview");
    
}

#pragma mark - IBAction method implementation

- (IBAction)showAlertView:(id)sender {
    [self.alertComponent showAlertViewWithSelectionHandler:^(NSInteger buttonIndex, NSString *buttonTitle) {
        NSLog(@"%ld, %@", (long)buttonIndex, buttonTitle);
    }];
}

- (IBAction)menuBtnTapped:(id)sender
{
    if (menuButton == YES) {
        menuButton = NO;
//        [self.menuComponent showMenuWithSelectionHandler:^(NSInteger selectedOptionIndex) {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"UIKit Dynamics Menu"
//                                                            message:[NSString stringWithFormat:@"You selected option #%d", selectedOptionIndex + 1]
//                                                           delegate:nil
//                                                  cancelButtonTitle:nil
//                                                  otherButtonTitles:@"Okay", nil];
//            [alert show];
//            
//        }];
//        showMenuGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(showMenu:)];
//        
//        showMenuGesture.direction = UISwipeGestureRecognizerDirectionRight;
//        
//        [self performSelector:@selector(showMenu:) withObject:showMenuGesture];
    }
    else
    {
        menuButton = YES;
        [self.menuComponent toggleMenu];
        
    }
    
    
    
}

- (IBAction)leftMenu:(id)sender {
    
    if (menuButton == YES) {
        menuButton = NO;
    NSLog(@"left menu YES");
         [self performSelector:@selector(showMenu:) withObject:showMenuGesture];
    }
    else
    {
        menuButton = YES;
        
        [self.menuComponent toggleMenu];
        self.menuComponent.isMenuShown = NO;
        
        NSLog(@"left menu NO");
        
    }

}



@end
